<?php

use Illuminate\Database\Seeder;
use App\Models\SiteLanguage;

class LangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_languages')->delete();

        SiteLanguage::create([
            'name' => 'en',
            'title' => 'English',
        ]);

        SiteLanguage::create([
            'name' => 'de',
            'title' => 'German',
        ]);

        SiteLanguage::create([
            'name' => 'es',
            'title' => 'Spanish',
        ]);
    }
}
