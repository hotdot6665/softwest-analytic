<?php

use Illuminate\Database\Seeder;
use App\Models\SiteCategory;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_categories')->delete();

        SiteCategory::create([
            'name_en' => 'All',
            'name_de' => 'Alles',
            'slug' => 'all-sites',
            'active' => 1,
            'popularity' => 99,
        ]);

        SiteCategory::create([
            'name_en' => 'Images',
            'name_de' => 'Bilder',
            'slug' => 'images',
            'active' => 1,
            'popularity' => 3,
        ]);

        SiteCategory::create([
            'name_en' => 'News',
            'name_de' => 'Nachrichten',
            'slug' => 'news',
            'active' => 1,
            'popularity' => 3,
        ]);

        SiteCategory::create([
            'name_en' => 'Videos',
            'name_de' => 'Videos',
            'slug' => 'videos',
            'active' => 1,
            'popularity' => 3,
        ]);

        SiteCategory::create([
            'name_en' => 'Shops',
            'name_de' => 'Geschäfte',
            'slug' => 'shops',
            'active' => 1,
            'popularity' => 3,
        ]);

    }
}
