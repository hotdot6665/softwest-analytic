<?php

use Illuminate\Database\Seeder;
use App\Models\Domain;

class DomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_domains')->delete();

        Domain::create([
            'name' => 'kevin.de',
            'main_lang' => 'en',
        ]);

        Domain::create([
            'name' => 'softwest.net',
            'main_lang' => 'en',
        ]);

        Domain::create([
            'name' => 'lutsk.rayon.in.ua',
            'main_lang' => 'ua',
        ]);
    }
}
