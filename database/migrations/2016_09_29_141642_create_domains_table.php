<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_domains', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('user_id');
            $table->string('name');
            $table->integer('timezone_id');
            $table->integer('site_category_id');
            $table->string('main_lang');
            // seo block
            $table->string('title_en', 300);
            $table->string('title_de', 300);
            $table->string('keyword_en', 300);
            $table->string('keyword_de', 300);
            $table->string('description_en', 500);
            $table->string('description_de', 500);

            $table->boolean('active');
            $table->boolean('code_active');
            $table->boolean('is_stat_visible');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_domains');
    }
}
