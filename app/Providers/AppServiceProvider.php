<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Models\SiteCategory;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('input_search', '');
        view()->share('input_lang', 'en');
        View::share('dev_name', 'dspire');
        // 
        //$rows = SiteCategory::active()->getByLang('en')->byPopularity()->get();
        // view()->share('siteCategories', $rows);
        $rows = SiteCategory::active()->byPopularity()->get();
        view()->share('siteCategories', $rows->toArray());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
