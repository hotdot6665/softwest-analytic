<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class VisitorActivity extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'visitor_activities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'visitor_cookie_id','time','domain',
    	'page_url','referrer','action_type',
    	'object_state','visitor_ip',
    ];
}
