<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'site_categories';

    /**
     * Scope a query to only include active
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByLang($query, $lang)
    {
    	$col_name = 'name_'.$lang;

        return $query->select('slug', $col_name.' as name');
    }

     /**
     * Scope a query to order by popular
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByPopularity($query)
    {
        return $query->orderBy('popularity', 'desc');
    }
}
