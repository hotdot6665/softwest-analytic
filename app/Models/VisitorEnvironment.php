<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class VisitorEnvironment extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'visitor_environments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'visitor_cookie_id', 'user_agent', 'time',
    	'domain','os','browser',
	    'page_url','referrer','screen_width',
	    'screen_height','screen_color_depth','timezone',
	    'visitor_ip','is_cookies','browser_language',
    ];


    /**
     * List of unique visitors
     */
    public function getUniqueVisitors()
    {
      $rows = DB::table('visitor_environments')
      ->select('*')
      ->where('os', '<>', '')->where('os', '<>', '-')
      ->where('user_agent', 'not like', '%bot%')
      ->where('is_cookies','=',1)
      ->groupBy('visitor_cookie_id')
      ->orderBy('created_at');

      return $rows;
    }

    /**
     * List of OS
     */
    public function getOS()
    {
      $rows = DB::table('visitor_environments')
      ->select(DB::raw('count(*) as os_count, os'))
      ->where('os', '<>', '')->where('os', '<>', '-')
      ->groupBy('os');

      return $rows;
    }

    /**
     * List of browser
     */
    public function getBrowser()
    {
      $rows = DB::table('visitor_environments')
      ->select(DB::raw('count(*) as browser_count, browser'))
      ->where('browser', '<>', '')
      ->where('browser', 'not like', '%bot%')
      ->groupBy('browser');

      return $rows;
    }
}
