<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\VisitorEnvironment;
use App\Models\VisitorActivity;
use Session;
use Cookie;


class GathererAnalyticsController extends Controller
{

    /**
     * Store a visitor env in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTechInfo(Request $request)
    {
        $cols_dictionary = [
            'userCookieId' => 'visitor_cookie_id',
            'time'      => 'time',
            'userAgent' => 'user_agent',
            'browser'   => 'browser',
            'OS'        => 'os',
            'domain'    => 'domain',
            'pageUrl'   => 'page_url',
            'referrer'  => 'referrer',
            'screenWidth' => 'screen_width',
            'screenHeight' => 'screen_height',
            'timezone'  => 'timezone',
            'screenColorDepth' => 'screen_color_depth',
            'cookiesEnabled'   => 'is_cookies',
            'browserLanguage'  => 'browser_language',
        ];

        $data = [];
        foreach ($cols_dictionary as $col_json => $col_db) {
            $data[$col_db] = trim($request->input($col_json, '-'));
        }
    	$data['visitor_ip'] = $_SERVER['REMOTE_ADDR'];    //$request->getClientIp()

        $answer = VisitorEnvironment::create($data);

        // set cookies
        $domain = $request->input('domain');
        $minutes = 60 * 24 * 7;     // == week
        Cookie::queue('visitorEnvironment', 1, $minutes, "/", $domain, false, false);
        
        return response()
            ->json(['key' => 'tech'])
            ->setCallback($request->input('callback'));
    }

    /**
     * Store visitor activity
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeActivity(Request $request)
    {
        $cols_dictionary = [
            'userCookieId' => 'visitor_cookie_id',
            'time'      => 'time',
            'pageUrl'   => 'page_url',
            'referrer'  => 'referrer',
            'action'    => 'action_type',
            'link-href' => 'object_state',
        ];

        $data = [];
        foreach ($cols_dictionary as $col_json => $col_db) {
            $data[$col_db] = trim($request->input($col_json, '-'));
        }
        $data['visitor_ip'] = $request->getClientIp();

        $answer = VisitorActivity::create($data);
        
        return response()
            ->json(['key' => 'activity'])
            ->setCallback($request->input('callback'));
    }
}

// - escaping