<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\VisitorEnvironment as VisitorEnvironment;
use App\Models\VisitorActivity;
use App\Models\SiteCategory;
use Session;
use GeoIP;


class PresentationsController extends Controller
{
    /**
      * List of OS which visitor used
      *
      * 
      */ 
    public function getOS(VisitorEnvironment $visitorEnv)
    {
        $records = $visitorEnv->getOS()->get();

        $pieces = array_chunk($records, 12);
        //dd($blks);

        return view('dump-page.about-os', ['pieces' => $pieces]);
    }

    /**
      * List of browser which visitor used
      *
      * 
      */ 
    public function getBrowser(VisitorEnvironment $visitorEnv)
    {
        $records = $visitorEnv->getBrowser()->get();

        $pieces = array_chunk($records, 12);

        return view('dump-page.about-browser', ['pieces' => $pieces]);
    }

    /**
      * ...
      *
      */ 
    public function getUniqueVisitors(VisitorEnvironment $visitorEnv)
    {
      // total: 20
      $captions = [
        'Domain', 'Date', 'IP', 'Country', 'OS'
      ];
      //$records = $visitorEnv->getUniqueVisitors()->take(20)->get();
      $records = $visitorEnv->getUniqueVisitors()->paginate(20);

      return view('dump-page.unique-visitors', [
          'visitors' => $records, 'captions' => $captions
        ]);
    }


    /**
      * ...
      *
      */ 
    public function getTechInfoByDomain()
    {
        $row = VisitorEnvironment::where('is_cookies', 1)->first();
        $record  = $row->toArray();
        $amount = count($record);
        //dd($record);
        return view('dump-page.info', ['visitorInfo' => $record, 'amount' => $amount]);
    }

    /**
      * ...
      *
      */ 
    public function getCategories()
    {
        $row = SiteCategory::active()->getByLang('en')->byPopularity()->get();
        dd($row->toArray());
    }
    
    public function getIP()
    {
        //geoip($ip = null);
        dd(geoip('130.180.208.178'));
    }
}


//
//$captions = [
//       'date', 'user agent', 'OS',
//       'screen width', 'screen height', 'IP',
//       'time zone', 'browser language', 'cookies',
//       'javascript',
//     ];