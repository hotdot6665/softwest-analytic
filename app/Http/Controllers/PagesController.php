<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\SiteCategory;

use LaravelGettext;
use App;


class PagesController extends Controller
{
    public function __construct() {
        //View::share('userSearchQuery', '');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'en')
    {
        LaravelGettext::setLocale($this->getLocale($lang));

        return view('main', [
                'input_lang' => $lang
            ]);
    }

    /**
     * ...
     *
     * @return \Illuminate\Http\Response
     */
    public function search($lang = 'en', Request $request)
    {
        LaravelGettext::setLocale($this->getLocale($lang));

        $str = $request->input('search');
        return view('search', [
                'input_search' => $str,
                'input_lang' => $lang,
            ]);
    }
    
    /**
     * ...
     *
     * @return \Illuminate\Http\Response
     */
    public function payment($lang = 'en')
    {
        LaravelGettext::setLocale($this->getLocale($lang));

        return view('payment', [
                'input_lang' => $lang
            ]);
    }

    /**
     * ...
     *
     * @return \Illuminate\Http\Response
     */
    public function category($lang = 'en', $category)
    {
        dd($category);
        LaravelGettext::setLocale($this->getLocale($lang));

        return view('payment', [
                'input_lang' => $lang
            ]);
    }

    /**
     * ...
     *
     * @return
     */
    public function getLocale($lang = 'en')
    {   
        $localeStore = ['en' => 'en_US', 'de' => 'de_DE'];

        if (!array_key_exists($lang, $localeStore)) {
            // throw exception || log it
            return current($localeStore);
        }

        return $localeStore[$lang];
    }
}
