<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use LaravelGettext;
use App\Models\SiteCategory;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'en')
    {
        LaravelGettext::setLocale($this->getLocale($lang));
        // imitation
        $rows = SiteCategory::active()->byPopularity()->paginate(4);

        return view('dashboard', [
                'input_lang' => $lang, 'domains' => $rows
            ]);
    }

    /**
     * Show the application user settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function userSettings($lang = 'en')
    {
        LaravelGettext::setLocale($this->getLocale($lang));


        return view('user-settings', [
                'input_lang' => $lang
            ]);
    }

    /**
     * ...
     *
     * @return
     */
    public function getLocale($lang = 'en')         // as trait
    {   
        $localeStore = ['en' => 'en_US', 'de' => 'de_DE'];

        if (!array_key_exists($lang, $localeStore)) {
            // throw exception || log it
            return current($localeStore);
        }

        return $localeStore[$lang];
    }
}
