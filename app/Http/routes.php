<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Recommendations
// --all pages need a route name  ...look at the lang swicher

Route::get('/{lang?}', 'PagesController@index')
->where(['lang' => '(en|de)'])->name('main');

Route::get('/{lang}/search', 'PagesController@search')
->where(['lang' => '(en|de)'])->name('search');

Route::get('/{lang}/payment', 'PagesController@payment')
->where(['lang' => '(en|de)'])->name('payment');

Route::get('/{lang}/category/{category}/', 'PagesController@category')
->where(['lang' => '(en|de)', 'category' => '[a-z-]+'])->name('category');

Route::get('/{lang?}/dashboard', 'HomeController@index')
->where(['lang' => '(en|de)'])->name('dashboard');

Route::get('/{lang}/user-settings', 'HomeController@userSettings')
->where(['lang' => '(en|de)'])->name('user-settings');


Route::get('dump-os', 'PresentationsController@getOS');
Route::get('dump-browser', 'PresentationsController@getBrowser');
Route::get('dump-unique', 'PresentationsController@getUniqueVisitors');

Route::get('dump-category', 'PresentationsController@getCategories');


/* API */
Route::get('/api/save', 'GathererAnalyticsController@storeTechInfo');
Route::get('/api/user-activity', 'GathererAnalyticsController@storeActivity');

/* auth */
Route::auth();
Route::get('/home', 'HomeController@index');

/*	other 	*/
Route::get('/testgeoip', 'PresentationsController@getIP');
Route::get('/en/test', function () {
	return route('search', ['lang'=>'de']);
});

Route::get('dump-unique/{locale}', function ($locale) {
    //App::setLocale($locale);
    //dd(App::getLocale());

    $visitorEnv = new \App\Models\VisitorEnvironment();
	$captions = [
	    'Domain', 'Date', 'IP', 'Country', 'OS'
	];
	$records = $visitorEnv->getUniqueVisitors()->paginate(50);

	return view('dump-page.unique-visitors', [
		'visitors' => $records, 'captions' => $captions
	]);
});

Route::get('info', function () {
    phpinfo();
});

Route::get('welcome', function () {
    return view('welcome');
});

