@extends('layouts.home-base')

@section('title', 'Dashboard')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrams">
                <a href="/">Home</a>
                <span>Dashboard</span>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="title">
                Add Site
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard">
                <ul class="clearfix">
                    <li>SHOW</li>
                    <li class="star"></li>
                    <li><a href="#">All</a></li>
                </ul>
                <ul class="clearfix">
                    <li><a href="#">kevin.de</a></li>
                    <li class="star"></li>
                    <li><a href="#">REPORTS</a></li>
                    <li><a href="#">Edit</a></li>
                    <li><a href="#">Delete</a></li>
                </ul>
                <ul class="clearfix">
                    <li><a href="#">kevin.de</a></li>
                    <li class="star"></li>
                    <li><a href="#">REPORTS</a></li>
                    <li><a href="#">Edit</a></li>
                    <li><a href="#">Delete</a></li>
                </ul>
                <ul class="clearfix">
                    <li><a href="#">kevin.de</a></li>
                    <li class="star"></li>
                    <li><a href="#">REPORTS</a></li>
                    <li><a href="#">Edit</a></li>
                    <li><a href="#">Delete</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="str">
                <!-- <div class="title">Domains</div> -->
                {{ $domains->links() }}
                <!-- <ul class="pagination">
                    <li class="disabled"><span>«</span></li>
                    <li class="active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">»</a></li>
                </ul> -->
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    console.log("!!!");
</script>

<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn_1' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn_1" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn_1"></span><span class="mybtn_1"></span><span class="mybtn_1"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn_1"></span><span class="mybtn"></span><span class="mybtn_1"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.star').click(function() {
            if ($(this).hasClass('on')) {
                // удаление
                $(this).removeClass('on');
            } else {
                // добавление
                $(this).addClass('on');
            };
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#addfav').click(function(){
            $('.favorit_add').attr('style','display:block;');
        });
        $('#submitfav').click(function(event){
            event.preventDefault()
            var val = $('#input_fav').val();
            $('.favorit').append('<div class="element"><a href="'+val+'">'+val+'</a><div onclick="del($(this));" class="delete"></div></div>');
            $('#input_fav').val('');
            //    $('.favorit_add').attr('style','display:none;');
            $('.favorit').attr('style', 'display:block;');
        });
    });
    function del(elem) {
        elem.parent().remove();
        console.log($('.element').length);
        if($('.element a').length == 0)
            $('.favorit').attr('style','display:none;');
    }
</script>
@endsection
