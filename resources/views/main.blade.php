@extends('layouts.base')

@section('title', 'Analyze your webpages now!')

@section('content')
<div class="container">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="favorit">
            <div class="element"></div>
        </div>
        <div id="myModal_1" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"></div>
                    <div class="modal-body">
                        <form class="favorit_add" action="">
                            <input id="input_fav" type="text" name=""/>
                            <button id="submitfav" type="submit">{{ _('add') }}</button>
                        </form>
                        <button class="close" type="button" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">{{ _('Закрыть') }}</button></div>
                </div>
            </div>
        </div>
        <button id="addfav" class="btn btn-info btn-lg" type="button" data-toggle="modal" data-target="#myModal_1">{{ _('add_favorit') }}</button>
        <div id="myModal_add_site" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal">×</button>
                <div class="modal-header">
                    <div class="title">
                        {{ _('Add site') }}
                    </div>
                    <form class="myModal_add_site" action="">
                        <div>
                            <label for="domain">{{ _('Domain') }}:</label>
                            <input name="text" id="domain" type="text" placeholder='Domain'/>
                        </div>
                    </form>
                    <div class="add_page">
                        <a href="#">{{ _('Add_page') }}</a>
                    </div>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="time_zone">{{ _('Time_zone') }}<span>*</span></label>
                            <select id="time_zone" class="form-control">
                                <option>{{ _('select1') }}</option>
                                <option>{{ _('select2') }}</option>
                                <option>{{ _('select3') }}</option>
                                <option>{{ _('select4') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="site_language">{{ _('Site_language') }}<span>*</span></label>
                            <select id="site_language" class="form-control">
                                <option>{{ _('select1') }}</option>
                                <option>{{ _('select2') }}</option>
                                <option>{{ _('select3') }}</option>
                                <option>{{ _('select4') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="site_category">{{ _('Site_category') }}<span>*</span></label>
                            <select id="site_category" class="form-control">
                                <option>{{ _('select1') }}</option>
                                <option>{{ _('select2') }}</option>
                                <option>{{ _('select3') }}</option>
                                <option>{{ _('select4') }}</option>
                            </select>
                        </div>
                    </form>
                    <div class="social_network">
                        <a class="social_change" data-plus="minus" href="#" onclick="openbox('next'); return false">{{ _('Social network') }}:</a>
                        <div id="next" style="display: block;">
                            <form class="myModal_social_network" action="">
                                <div class="clearfix">
                                    <label for="facebook">Facebook:</label>
                                    <input name="text" id="facebook" type="text"/>
                                </div>
                                <div class="clearfix">
                                    <label for="twitter">Twitter:</label>
                                    <input name="text" id="twitter" type="text"/>
                                </div>
                                <div class="clearfix">
                                    <label for="google">Google+:</label>
                                    <input name="text" id="google" type="text"/>
                                </div>
                                <div class="clearfix">
                                    <label for="youtube">Youtube:</label>
                                    <input name="text" id="youtube" type="text"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tariff clearfix">
                        <div class="title">{{ _('Tariff plane') }}:</div>
                        <div class="tariff_left">
                            <div class="float_right_group">
                                <div class="tariff_title clearfix">
                                    <input type="radio" class="checkbox" id="tariff_title_left" />
                                    <label for="tariff_title_left">{{ _('Free') }}</label>
                                </div>
                                <div class="shows">
                                    <div class="shows_group">
                                        <div class="shows_image">
                                            <div class="shows_img">
                                                <img src="images/zaglushka.png" alt="img"/>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <input type="checkbox" class="checkbox" id="white" />
                                            <label for="white">{{ _('White') }}</label>
                                        </div>
                                    </div>
                                    <div class="shows_group">
                                        <div class="shows_image">
                                            <div class="shows_img">
                                                <img src="images/zaglushka.png" alt="img"/>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <input type="checkbox" class="checkbox" id="black" />
                                            <label for="black">{{ _('Black') }}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="tariff_teazer">
                                    {{ _('Counter shows logo') }} 40x40
                                </div>
                                <div class="tariff_soseti clearfix">
                                    <div>
                                        <input type="checkbox" class="checkbox" id="tariff_facebook" />
                                        <label for="tariff_facebook">Facebook</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" class="checkbox" id="tariff_twitter" />
                                        <label for="tariff_twitter">Twitter</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" class="checkbox" id="tariff_youtube" />
                                        <label for="tariff_youtube">Youtube</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" class="checkbox" id="tariff_google" />
                                        <label for="tariff_google">Google+</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" class="checkbox" id="tariff_pagerank" />
                                        <label for="tariff_pagerank">Pagerank</label>
                                    </div>
                                </div>
                                <div class="soseti_like_group">
                                    <div class="like_image clearfix">
                                        <div class="like_img">
                                            <img src="images/zaglushka.png" alt="img"/>
                                        </div>
                                    </div>
                                    <div class="social-likes social-likes_vertical">
                                        <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
                                        <div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
                                        <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
                                        <div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tariff_bottom">
                                <div>
                                    {{ _('Social network icons shows near the logo') }}
                                </div>
                                <div>
                                    {{ _('Statistics of your website will be visible') }}
                                </div>
                            </div>
                        </div>
                        <div class="tariff_right clearfix">
                            <div class="tariff_title clearfix">
                                <input type="radio" class="checkbox" id="tariff_title_right" />
                                <label for="tariff_title_right">{{ _('Private') }}</label>
                            </div>
                            <div class="privat_group clearfix">
                                <div class="privat_image">
                                    <div class="privat_img">
                                        <img src="images/zaglushka.png" alt="img"/>
                                    </div>
                                </div>
                                <div class="privat_right clearfix">
                                    <div>
                                        <input type="radio" class="checkbox" id="domain_1" />
                                        <label for="domain_1">1 {{ _('domain') }}.....5$</label>
                                    </div>
                                    <div>
                                        <input type="radio" class="checkbox" id="domain_2" />
                                        <label for="domain_2">2 {{ _('domain') }}.....5$</label>
                                    </div>
                                    <div>
                                        <input type="radio" class="checkbox" id="domain_3" />
                                        <label for="domain_3">3 {{ _('domain') }}.....5$</label>
                                    </div>
                                    <div>
                                        <input type="radio" class="checkbox" id="domain_4" />
                                        <label for="domain_4">4 {{ _('domain') }}.....5$</label>
                                    </div>
                                </div>
                            </div>
                            <div class="tariff_teazer">
                                {{ _('Counter without logo') }}
                            </div>
                            <div class="tariff_bottom">
                                <div>
                                    {{ _('Statistics of your website will be invisible') }}
                                </div>
                            </div>
                            <div class="payment_button">
                                <button>{{ _('Payment') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="getcounter">{{ _('Get Counter Code') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
        <button id="addfav" class="btn btn-info btn-lg" type="button" data-toggle="modal" data-target="#myModal_add_site">add_site</button>
    </div>
</div>
</div>
<div class="container">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="features">
            <div class="title">{{ _('Features') }}</div>
            <div class="features_block_left">
                <div class="features_block_left_img">
                    <div class="images">
                        <div class="img">
                            <img src="images/img_1.png" alt="img"/>
                        </div>
                        <div class="images_title">{{ _('Summary') }}</div>
                    </div>
                </div>                                                   
                <div class="teazer">
                    {{ _('Totals and Averages') }}
                </div>
            </div>
            <div class="features_block_right">
                <div class="features_block_right_img">
                    <div class="images">
                        <div class="img">
                            <img src="images/img_2.png" alt="img"/>
                        </div>
                        <div class="images_title">{{ _('Tracking') }}</div>
                        <div class="images_spisok">
                            <p>{{ _('Days') }},</p>
                            <p>{{ _('Weeks') }},</p>
                            <p>{{ _('Months') }}</p>
                            <p>{{ _('Hours of the day') }},</p>
                            <p>{{ _('Days of the Week') }}</p>
                        </div>
                    </div>
                </div>
                <div class="teazer">
                   {{ _('Unique Visitors') }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="why">
            <div class="title">
                {{ _('Why us') }}
            </div>
            <div class="why_menu clearfix">
                <ul class="why_left">
                    <li>
                        <p>{{ _('Real-time reporting') }}</p>
                    </li>
                    <li>
                        <p>{{ _('Wide range of specifications') }}</p>
                    </li>
                    <li>
                        <p>{{ _('Extensive referrer tracking') }}</p>
                    </li>
                    <li>
                        <p>{{ _('Java-script optimized(5 times faster)') }}</p>
                    </li>
                </ul>
                <ul class="why_right">
                    <li>
                        <p>{{ _('You time zone') }}</p>
                    </li>
                    <li>
                        <p>{{ _('No traffic limitations') }}</p>
                    </li>
                    <li>
                        <p>{{ _('Setup withing a few minutes') }}</p>
                    </li>
                    <li>
                        <p>{{ _('Completely FREE') }}</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="info clearfix">
            <div class="info_container">
                <div class="info_left">
                    <div class="">
                        <p>{{ _('For Independent') }}<br/> {{ _('Webmaster') }}</p>
                    </div>
                </div>
                <div class="info_middle">
                    <div class="">
                        <p>{{ _('For Companies Managing') }}<br/> {{ _('For Companies Managing') }}x</p>
                    </div>
                </div>
                <div class="info_right">
                    <div class="">
                        <p>{{ _('For SEO Agencies') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.star').click(function() {
            if ($(this).hasClass('on')) {
                // удаление
                $(this).removeClass('on');
            } else {
                // добавление
                $(this).addClass('on');
            };
        });
    });
</script>
<script>
    $(document).ready(function(){
    $('#addfav').click(function(){
    $('.favorit_add').attr('style','display:block;');
    });
    $('#submitfav').click(function(event){
    event.preventDefault()
    var val = $('#input_fav').val();
    $('.favorit').append('<div class="element"><a href="'+val+'">'+val+'</a><div onclick="del($(this));" class="delete"></div></div>');
    $('#input_fav').val('');
    //    $('.favorit_add').attr('style','display:none;');
    $('.favorit').attr('style', 'display:block;');
    });
    });
    function del(elem) {
    elem.parent().remove();
    console.log($('.element').length);
    if($('.element a').length == 0)
    $('.favorit').attr('style','display:none;');
    }
</script>
@endsection