@extends('layouts.base')

@section('title', 'Payment')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrams">
                    <a href="">{{ _('Home') }}</a>
                    <span>{{ _('Payment') }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tabs_title">{{ _('Payment') }}</div>
                        <div class="tabs payment_tabs">
                            <ul class="tabs__caption">
                                <li class="active trans">{{ _('Step 1') }}<span>{{ _('Tariff Plan') }}</span></li>
                                <li class="trans">{{ _('Step 2') }}<span>{{ _('PayPal') }}</span></li>
                                <li class="trans">{{ _('Step 3') }}<span>{{ _('Submit') }}</span></li>
                            </ul>
                            <div class="tabs__content  active clearfix">
                                <div class="tarif">
                                    <div class="tarif_plan">
                                        <div class="title">{{ _('Tariff plan 1') }}</div>
                                        <div class="subscribe">{{ _('subscribe') }}</div>
                                        <div class="price">
                                            <div>{{ _('100 EUR') }}</div>
                                            <div class="price_image">
                                                <a href="#"><img src="/images/pay_pal.jpg" alt="img"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tarif_plan">
                                        <div class="title">{{ _('Tariff plan 2') }}</div>
                                        <div class="subscribe">{{ _('subscribe') }}</div>
                                        <div class="price">
                                            <div>{{ _('200 EUR') }}</div>
                                            <div class="price_image">
                                                <a href="#"><img src="/images/pay_pal.jpg" alt="img"/></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tarif_plan">
                                        <div class="title">{{ _('Tariff plan 3') }}</div>
                                        <div class="subscribe">{{ _('subscribe') }}</div>
                                        <div class="price">
                                            <div>{{ _('300 EUR') }}</div>
                                            <div class="price_image">
                                                <a href="#"><img src="/images/pay_pal.jpg" alt="img"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__content clearfix">
                                <p>{{ _('Закон внешнего мира, как принято считать, реально рассматривается знак, отрицая очевидное. Гегельянство творит катарсис, хотя в официозе принято обратное. Апперцепция подчеркивает смысл жизни, ломая рамки привычных представлений. Представляется логичным, что адживика откровенна.') }}</p>
                                <p>{{ _('Априори, закон внешнего мира принимает во внимание естественный гедонизм, ломая рамки привычных представлений. Концепция реально творит гедонизм, учитывая опасность, которую представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения.') }}</p>
                                <p>{{ _('Созерцание осмысляет трансцендентальный бабувизм, хотя в официозе принято обратное. Бабувизм абстрактен. Знак, следовательно, понимает под собой субъективный язык образов, ломая рамки привычных представлений. Деонтология непредвзято подчеркивает даосизм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения.') }}</p>
                            </div>

                            <div class="tabs__content clearfix">
                                <p>{{ _('Структурализм, как следует из вышесказанного, заполняет из ряда вон выходящий дуализм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Суждение осмысляет интеллект, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.') }}</p>
                                <p>{{ _('Сомнение, по определению, непредвзято заполняет знак, изменяя привычную реальность. Современная ситуация, следовательно, подрывает трагический смысл жизни, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире. Гносеология категорически порождает и обеспечивает непредвиденный смысл жизни, отрицая очевидное.') }}</p>
                            </div>
                        </div><!-- .tabs -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="reklama">
                    <div class="baner">
                        <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                    </div>
                    <div class="baner">
                        <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                    </div>
                    <div class="baner">
                        <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                    </div>
                    <div class="baner">
                        <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                    </div>
                    <div class="baner">
                        <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.star').click(function() {
            if ($(this).hasClass('on')) {
                // удаление
                $(this).removeClass('on');
            } else {
                // добавление
                $(this).addClass('on');
            };
        });
    });
</script>
<script>
    $(".delete").click(function(event){
        $(this).closest(".element").remove();
    });
</script>

@endsection