
<input name="name" type="text" placeholder='Benutzer name'
    value="{{ old('name') }}" />
@if ($errors->has('name'))
    <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
@endif

<input name="email" type='email' placeholder='Email'
    value="{{ old('email') }}" />
@if ($errors->has('email') AND old('register') !== null)
    <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif

<!-- <input name="country" type="text" placeholder='Country'
    value="{{ old('country') }}" /> -->

<select name="country" class="selectpicker">
    <option>England</option>
    <option>Germany</option>
    <option>Poland</option>
</select>

<select name="personType" class="selectpicker">
    <option>Legal Person</option>
    <option>Natural Person</option>
    <option>Organisator</option>
</select>

<input type="password" maxlength="25" size="40" name="password"
    placeholder='Password'/>
@if ($errors->has('password') AND old('register') !== null)
    <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif

<div class="capcha">
    {!! captcha_img() !!}
    <p><input type="text" name="captcha" placeholder="Enter you captcha"></p>
</div>
<button class="register" type="submit"
    name="register" value="1">Register</button>
