@extends('layouts.dump')

@section('title', 'Operating systems')

@section('content')

<div class="infoGroupx">
@foreach ($pieces as $items)
	<ul>
    @foreach ($items as $item)
        <li>{{ $item->os }} <span style="padding-left: 10px;">({{ $item->os_count }})</span></li>
    @endforeach 
	</ul>
@endforeach 
</div>

@endsection