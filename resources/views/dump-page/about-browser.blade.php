@extends('layouts.dump')

@section('title', 'Visitor Browsers')

@section('content')

<div class="infoGroupx">
@foreach ($pieces as $items)
	<ul>
	@foreach ($items as $item)
    	<li>{{ $item->browser }}</li>
    @endforeach 
	</ul>
@endforeach 
</div>

@endsection