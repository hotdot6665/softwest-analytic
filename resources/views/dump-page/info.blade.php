@extends('layouts.dump')

@section('title', 'Tech Info')

@section('content')

<div>
<table class="pure-table pure-table-horizontal">
    <thead>
    <tr>
    @foreach ($visitorInfo as $key => $item)
    	<th>#</th>
    @endforeach  
    </tr>
    </thead>
	
    <tbody>
    <tr>
    @foreach ($visitorInfo as $key => $item)
    	<td>{{ $item }}</td>
    @endforeach  
    </tr>

    </tbody>
</table>

</div>

@endsection