@extends('layouts.dump')

@section('title', 'Unique Visitors')

@section('content')

<?php $i = 0; ?>
<div>
<table class="pure-table pure-table-horizontal">
    <thead>
    <tr>
        <th>№</th>
    @foreach ($captions as $key => $item)
    	<th>{{ $item }}</th>
    @endforeach
    </tr>
    </thead>
	
    <tbody>
    @foreach ($visitors as $key => $item)
    <?php 
    $date = date("d.m.y D H:i:s", strtotime($item->created_at));
    $i++;
    ?>
    <tr>
        <td>{{ $i }}</td>
        <td>{{ $item->domain }}</td>
    	<td>{{ $date }}</td>
        <td>{{ $item->visitor_ip }}</td>
        <td>Ukraine</td>
        <td>{{ $item->os }}</td>
    </tr>
    @endforeach
    </tbody>
</table>

{{ $visitors->links() }}

</div>

@endsection