
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="clearfix">
                        <li><a href="#">{{ _('AGB') }}</a></li>
                        <li><a href="#">{{ _('FAQ') }}</a></li>
                        <li><a href="#">Kontact</a></li>
                        <li><a href="#">Impressum</a></li>
                        <li><a href="#">Datenschutzbedienungen</a></li>
                        <li><a href="#">Nutzungbedienungen</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="footer_left">
                        <div class="title">Join our newsletter</div>
                        <div class="subscribe">
                            <form action="">
                                <input name='' type='email' value='' placeholder="enter you e-mail address"/>
                                <button type="submit">{{ _('subscribe') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="footer_right">
                        <div class="title">{{ _('Keep updated') }}</div>
                        <div class="soseti">
                            <a href="#"><img src="{{ URL::asset('images/twitter.png') }}" alt="twitter"/></a>
                            <a href="#"><img src="{{ URL::asset('images/facebook.png') }}" alt="facebook"/></a>
                            <a href="#"><img src="{{ URL::asset('images/google.png') }}" alt="google"/></a>
                            <a href="#"><img src="{{ URL::asset('images/instagram.png') }}" alt="instagram"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>