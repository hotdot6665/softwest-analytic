<div class="container" id="header_login">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="header header_log clearfix">
                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 widht_100_xs">
                    <div class="logo">
                        <a href="/"><img src="/images/zaglushka.png" alt="logo"/></a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 widht_100_xs">
                    <div class="title">{{ _('analyze your webpages now!') }}</div>
                    <div class="header_right">
                        <div class="language">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                    {{ $input_lang }}
                                </button>
                                <ul class="dropdown-menu">
                                @if ($input_lang == 'en')
                                    <li><a href="{{ route(Route::currentRouteName(), ['lang'=>'de']) }}">de</a></li>
                                @else
                                    <li><a href="{{ route(Route::currentRouteName(), ['lang'=>'en']) }}">en</a></li>
                                @endif
                                </ul>
                            </div>
                        </div>
                        <div class="setting_group">
                            <div class="upgrade">
                                <div class="upgrade_title">
                                    <div>{{ _('Free') }} <span><a href="#">{{ _('Upgrade') }}</a></span></div>
                                </div>
                            </div>
                            <div class="setting">
                                <div class="message">
                                    <div><span>2</span></div>
                                </div>
                                <div class="user_set"><a href="#"></a></div>
                            </div>
                            <div class="use_menu clearfix">
                                <ul class="clearfix">
                                    <li><a href="#">{{ Auth::user()->email }}</a></li>
                                    <li><a href="/logout">{{ _('Log out') }}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="header_top_menu">
                        <ul>
                            <li>{{ _('Show') }}</li>
                            <li class="star"></li>
                        <?php
                        $cloneCategories = $siteCategories;
                        $first_site_category = array_shift($siteCategories);
                        ?>
                            <li class="cont">
                                <a href="{{ route('category', ['lang'=>$input_lang, 'category' => $first_site_category['slug'] ])
                                    }}">{{ $first_site_category['name_'.$input_lang] }}</a>
                            </li>
                        @foreach ($siteCategories as $item)
                            <li>
                                <a href="{{ route('category', ['lang'=>$input_lang, 'category' => $item['slug'] ]) }}">{{ $item['name_'.$input_lang] }}</a>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-lg-1 col-md-1 col-sm-3 col-xs-3">
                    <div class="top_menu_respons">
                        <div class="material-menu-button top_menu_respons_button" id="mybtn_1" data-id="cls">
                            <span class="mybtn"></span>
                            <span class="mybtn"></span>
                            <span class="mybtn"></span>
                        </div>
                    </div>
                </div>
                <div class="material-menu" style="display: none;">
                    <nav class="vertical">
                        <div class="material-menu-wrapper material-menu-view-mobile" style="some:style;">
                        @foreach ($cloneCategories as $item)
                            <div class="material-menu-wrapper_group">
                                <div class="material-menu_right">
                                    <a href="{{ route('category', ['lang'=>$input_lang, 'category' => $item['slug']])
                                        }}">{{ $item['name_'.$input_lang] }}</a>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </nav>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-9 col-xs-9 widht_respons">
                    <div class="searh">
                        <form action="/{{ $input_lang }}/search" method="GET">
                            <input name='search' type="search" value="{{ $input_search }}"
                                placeholder="kev"/>
                            <button type="submit">{{ _('search') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>