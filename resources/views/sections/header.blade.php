<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!-- start: modal -->
        <div id="myModal_reg" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        {{ _('Register to add sites group to favorites') }}
                    </div>
                    <div class="modal-body">
                        <form class="modal_reg" method="POST" action="{{ url('/register?lang='.$input_lang) }}">
                            {{ csrf_field() }}

                            @include('forms.registration')

                        </form>
                    </div>
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <div class="modal-footer">
                        <div>
                            {{ _('By clicking on "REGISTER", you are agreeing to our ') }}  
                        </div>
                       <div>
                           <a href="#">{{ _('Terms of Use') }}</a>
                       </div>
                    </div>
                </div>
                <div class="modal-footer_bottom">
                    <div class="title">{{ _('Already Registered?') }}
                        <span>{{ _('Login into your accound') }}</span>
                    </div>
                    <form class="modal_reg_bottom" method="POST" action="{{ url('/login?lang='.$input_lang) }}">
                        {{ csrf_field() }}

                        @if ($errors->has('email') AND old('register') == null)
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        @if ($errors->has('password') AND old('register') == null)
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <input name="email" type='email' value='' placeholder='Email'/>
                        <input type="password" maxlength="25" size="40" name="password" placeholder='Password'/>
                        <button type="submit" name="login" value="1">{{ _('Log in') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end: modal -->
        <div class="header clearfix">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 widht_100_xs">
                <div class="logo">
                    <a href="/"><img src="/images/zaglushka.png" alt="logo"/></a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8 widht_100_xs">
                <div class="title">{{ _('analyze your webpages now!') }}</div>
                <div class="header_right">
                    <div class="language">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                {{ $input_lang }}
                            </button>
                            <ul class="dropdown-menu">
                                @if ($input_lang == 'en')
                                <li><a href="{{ route(Route::currentRouteName(), ['lang'=>'de']) }}">de</a></li>
                                @else
                                <li><a href="{{ route(Route::currentRouteName(), ['lang'=>'en']) }}">en</a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="login">
                    @if (Auth::guest())
                        <a href="#" class="btn btn-info btn-lg" type="button"
                            data-toggle="modal" data-target="#myModal_reg">{{ _('Log in') }}</a>
                    @else
                        <a href="/logout" class="btn btn-info btn-lg"
                            type="button">log out ({{ Auth::user()->name }})</a>
                    @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="header_top_menu">
                    <ul>
                        <li>{{ _('Show') }}</li>
                        <li class="star"></li>
                    <?php
                    $cloneCategories = $siteCategories;
                    $first_site_category = array_shift($siteCategories);
                    ?>
                        <li class="cont">
                            <a href="{{ route('category', ['lang'=>$input_lang, 'category' => $first_site_category['slug'] ])
                                }}">{{ $first_site_category['name_'.$input_lang] }}</a>
                        </li>
                    @foreach ($siteCategories as $item)
                        <li>
                            <a href="{{ route('category', ['lang'=>$input_lang, 'category' => $item['slug'] ]) }}">{{ $item['name_'.$input_lang] }}</a>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>

            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                <div class="top_menu_respons">
                    <div class="material-menu-button top_menu_respons_button" id="mybtn" data-id="cls">
                        <span class="mybtn"></span>
                        <span class="mybtn"></span>
                        <span class="mybtn"></span>
                    </div>
                </div>
            </div>
            <div class="material-menu" style="display: none;">
                <nav class="vertical">
                    <div class="material-menu-wrapper material-menu-view-mobile" style="some:style;">
                    @foreach ($cloneCategories as $item)
                        <div class="material-menu-wrapper_group">
                            <div class="material-menu_right">
                                <a href="{{ route('category', ['lang'=>$input_lang, 'category' => $item['slug']])
                                    }}">{{ $item['name_'.$input_lang] }}</a>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </nav>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-11 col-xs-11 widht_respons">
                <div class="searh">
                    <form action="/{{ $input_lang }}/search" method="GET">
                        <input name='search' type="search" value="{{ $input_search }}"
                            placeholder="kev"/>
                        <button type="submit">{{ _('search') }}</button>
                    </form>
                </div>
            </div>
        </div>

        </div>
    </div>
</div>