@extends('layouts.base')

@section('title', 'Searching result')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="main">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="favorit" style="display:block;">
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="favorit" style="display:block;">
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="favorit" style="display:block;">
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="favorit" style="display:block;">
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                            <div class="element">
                                <a>Текст11 текст текст текст текст</a>
                                <div class="delete"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrams">
                            <a href="">{{ _('Home') }}</a>
                            <span>{{ _('Searching results') }}</span>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="searching_results clearfix">
                            <div class="results_header">
                                <div class="results_header_top clearfix">
                                    <div class="results_header_left">
                                        <a href="#">kevin.de</a>
                                        <a href="#">kevin.de/</a>
                                    </div>
                                    <div class="results_header_right">
                                        <ul>
                                            <li class="star"></li>
                                            <li><a href="#">Statistic</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="results_header_bottom">
                                    <div class="title">
                                        Aufruf Ihrer Homepage als kevin.de
                                    </div>
                                    <div class="results_header_bottom_menu">
                                        <ul class="clearfix">
                                            <li><a href="#">AGB</a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Kontact</a></li>
                                            <li><a href="#">Impressum</a></li>
                                            <li><a href="#">Datenschutzbedienungen</a></li>
                                            <li><a href="#">Nutzungbedienungen</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="like">
                                <div class="like_top clearfix">
                                    <div class="like_rank">
                                        <span class="title">Traffic:</span>
                                        <span class="number">1000</span>
                                    </div>
                                    <div class="like_rank">
                                        <span class="title">Facebook Like:</span>
                                        <span class="number">10</span>
                                        <span class="number">K</span>
                                    </div>
                                    <div class="like_rank">
                                        <span class="title">Google+ Like:</span>
                                        <span class="number">10</span>
                                        <span class="number">K</span>
                                    </div>
                                </div>
                                <div class="like_bottom clearfix">
                                    <div class="like_rank">
                                        <span class="title">Traffic:</span>
                                        <span class="number">1000</span>
                                    </div>
                                    <div class="like_rank">
                                        <span class="title">Facebook Like:</span>
                                        <span class="number">10</span>
                                        <span class="number">K</span>
                                    </div>
                                    <div class="like_rank">
                                        <span class="title">Google+ Like:</span>
                                        <span class="number">10</span>
                                        <span class="number">K</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="sidebar">
                    <div class="reklama">
                        <div class="baner">
                            <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                        </div>
                        <div class="baner">
                            <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                        </div>
                        <div class="baner">
                            <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                        </div>
                        <div class="baner">
                            <a href="#"><img src="/images/zaglushka.png" alt="img"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.star').click(function() {
            if ($(this).hasClass('on')) {
                // удаление
                $(this).removeClass('on');
            } else {
                // добавление
                $(this).addClass('on');
            };
        });
    });
</script>
<script>
    $(".delete").click(function(event){
        $(this).closest(".element").remove();
    });
</script>
@endsection
