@extends('layouts.home-base')

@section('title', 'Dashboard')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcrams">
                <a href="/">Home</a>
                <a href="{{ route('dashboard', ['lang' => $input_lang]) }}">Dashboard</a>
                <span>User settings</span>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <div class="user_set_login">
                    <form action="">
                        <div class="clearfix">
                            <label for="email">E-mail</label>
                            <input id="email" type="email" placeholder=""
                                value="{{ Auth::user()->email }}" />
                        </div>
                        <div class="clearfix">
                            <label for="password">Password</label>
                            <input id="password" type="password" placeholder="*******"
                                value="" />
                        </div>
                        <div class="clearfix">
                            <label for="new_password">New_password</label>
                            <input id="new_password" type="password" placeholder="*******"
                                value="" />
                        </div>
                        <div class="clearfix">
                            <label for="language">Language</label>
                            <select id="language" class="selectpicker">
                                <option value="en">English</option>
                                <option value="de">German</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="edit"><a href="#">Edit</a></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn_1' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn_1" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn_1"></span><span class="mybtn_1"></span><span class="mybtn_1"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn_1"></span><span class="mybtn"></span><span class="mybtn_1"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>
<script type="text/javascript">

    $(document).ready(function() {

        $('.star').click(function() {
            if ($(this).hasClass('on')) {
                // удаление
                $(this).removeClass('on');
            } else {
                // добавление
                $(this).addClass('on');
            };
        });
    });
</script>
@endsection
