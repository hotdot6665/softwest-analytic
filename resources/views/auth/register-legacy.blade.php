@extends('layout.base')

@section('title', 'Analyze your webpages now!')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="favorit">
                    <div class="element"></div>
                </div>
                <div id="myModal_1" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header"></div>
                            <div class="modal-body">
                                <form class="favorit_add" action="">
                                    <input id="input_fav" type="text" name=""/>
                                    <button id="submitfav" type="submit">add</button>
                                </form>
                                <button class="close" type="button" data-dismiss="modal">×</button>
                            </div>
                            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button></div>
                        </div>
                    </div>
                </div>
                <button id="addfav" class="btn btn-info btn-lg" type="button" data-toggle="modal" data-target="#myModal_1">add_favorit</button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="features">
                    <div class="title">Features</div>
                    <div class="features_block_left">
                        <div class="features_block_left_img">
                            <div class="images">
                                <div class="img">
                                    <img src="images/img_1.png" alt="img"/>
                                </div>
                                <div class="images_title">Summary</div>
                            </div>
                        </div>
                        <div class="teazer">
                            Totals and Averages
                        </div>
                    </div>
                    <div class="features_block_right">
                        <div class="features_block_right_img">
                            <div class="images">
                                <div class="img">
                                    <img src="images/img_2.png" alt="img"/>
                                </div>
                                <div class="images_title">Tracking</div>
                                <div class="images_spisok">
                                    <p>Days,</p>
                                    <p>Weeks,</p>
                                    <p>Months</p>
                                    <p>Hours of the day,</p>
                                    <p>Days of the Week</p>
                                </div>
                            </div>
                        </div>
                        <div class="teazer">
                           Unique Visitors
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="why">
                    <div class="title">
                        Why us
                    </div>
                    <div class="why_menu clearfix">
                        <ul class="why_left">
                            <li>
                                <p>Real-time reporting</p>
                            </li>
                            <li>
                                <p>Wide range of specifications</p>
                            </li>
                            <li>
                                <p>Extensive referrer tracking</p>
                            </li>
                            <li>
                                <p>Java-script optimized(5 times faster)</p>
                            </li>
                        </ul>
                        <ul class="why_right">
                            <li>
                                <p>You time zone</p>
                            </li>
                            <li>
                                <p>No traffic limitations</p>
                            </li>
                            <li>
                                <p>Setup withing a few minutes</p>
                            </li>
                            <li>
                                <p>Completely FREE</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="info clearfix">
                    <div class="info_container">
                        <div class="info_left">
                            <div class="">
                                <p>For Independent<br/> Webmaster</p>
                            </div>
                        </div>
                        <div class="info_middle">
                            <div class="">
                                <p>For Companies Managing<br/> Their Corporate Website</p>
                            </div>
                        </div>
                        <div class="info_right">
                            <div class="">
                                <p>For SEO Agencies</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script>
    $('body').on('click',function(e){
        if(e.target.id == 'mybtn' || $.inArray('mybtn', e.target.classList) == 0){
            if($('.top_menu_respons_button').attr('data-id') == 'cls'){
                $('.material-menu-button').html('<div class="mybtn" style="font-size: 30px; margin-top: -15px;color: #03c4eb;">x</div>');
                $('.material-menu-button').attr('data-id','opn');
                $('.material-menu').attr('style','display:block;');
            }else{
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }else{
            if($('.top_menu_respons_button').attr('data-id') == 'opn'){
                $('.material-menu-button').html('<span class="mybtn"></span><span class="mybtn"></span><span class="mybtn"></span>');
                $('.material-menu-button').attr('data-id','cls');
                $('.material-menu').attr('style','display:none;');
            }
        }
    });

</script>

<script type="text/javascript">

    $(document).ready(function() {

        $('.star').click(function() {
            if ($(this).hasClass('on')) {
                // удаление
                $(this).removeClass('on');
            } else {
                // добавление
                $(this).addClass('on');
            };
        });
    });
</script>
<script>
    $(document).ready(function(){
    $('#addfav').click(function(){
    $('.favorit_add').attr('style','display:block;');
    });
    $('#submitfav').click(function(event){
    event.preventDefault()
    var val = $('#input_fav').val();
    $('.favorit').append('<div class="element"><a href="'+val+'">'+val+'</a><div onclick="del($(this));" class="delete"></div></div>');
    $('#input_fav').val('');
    //    $('.favorit_add').attr('style','display:none;');
    $('.favorit').attr('style', 'display:block;');
    });
    });
    function del(elem) {
    elem.parent().remove();
    console.log($('.element').length);
    if($('.element a').length == 0)
    $('.favorit').attr('style','display:none;');
    }
</script>

<script type="text/javascript">
    $('.login a').trigger('click');
</script>
@endsection