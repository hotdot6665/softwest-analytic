<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- <meta http-equiv="refresh" content="5; url=http://analytics.loc/de/payment"> -->

    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/menu.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/develop.css') }}">

    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/tabs_click-to-activate.js') }}"></script>

</head>
<body>
<div class="wrapper">

{{-- include header --}}
@if (Auth::guest())
	@include('sections.header')
@else
  @include('sections.home-header')
@endif

{{-- include reclame --}}
	@include('sections.reclame')

@yield('content')

{{-- include footer --}}
	@include('sections.footer')

</div>

@yield('scripts')

@if (old('login') !== null OR old('register') !== null OR Request::input('login') === '') 
<script type="text/javascript">
    $('.login a').trigger('click');
</script>
@endif

<script type="text/javascript" src="http://analytics.loc/js/lib/js.cookie.js"></script>
<script type="text/javascript" src="http://analytics.loc/js/lib/detect.min.js"></script>
<script type="text/javascript">
// injection script  --google way
(function(i,s,o,g,r,a,m){
  a=s.createElement(o);
  m=s.getElementsByTagName(o)[0];
  a.async=1;
  a.src=g;
  m.parentNode.insertBefore(a,m);
})(window,document,'script','http://analytics.loc/js/statistics.js');
</script>

</body>
</html>