<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <title>@yield('title')</title>

<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
<style>
td {
	max-width: 225px;
	overflow: hidden !important;
    text-overflow: ellipsis;
    white-space: nowrap;
}
body {
    margin-left: 10px;
}
.infoGroupx ul {
    float: left;
    width: 20%;
    min-width: 210px;
}
</style>
</head>
<body>

<div class="pure-menu pure-menu-horizontal">
    <ul class="pure-menu-list">
    	<li class="pure-menu-item"><strong>Some Gathered Data:</strong>
    	</li>
        <li class="pure-menu-item">
        	<a href="/dump-unique" class="pure-menu-link">Unique Visitors</a>
        </li>
        <li class="pure-menu-item">
        	<a href="/dump-browser" class="pure-menu-link">All Browsers</a>
        </li>
        <li class="pure-menu-item">
        	<a href="/dump-os" class="pure-menu-link">All OS</a>
        </li>
    </ul>
</div>

<h3>@yield('title')</h3>

@yield('content')

</body>
</html>
