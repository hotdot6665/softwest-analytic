<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- <meta http-equiv="refresh" content="5; url=http://analytics.loc/de/payment"> -->

    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/menu.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/develop.css') }}">

    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.1.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/tabs_click-to-activate.js') }}"></script>

</head>
<body>
<div class="wrapper">

{{-- include header --}}
    @include('sections.home-header')

@yield('content')

{{-- include reclame --}}
  @include('sections.reclame')

{{-- include footer --}}
	@include('sections.footer')

</div>

@yield('scripts')

</body>
</html>