(function(){
// 
var analyticApi = {
    "domain": "http://analytics.loc/",
    //"domain": "http://wa-dev.softwest.net/",
    get addTechInfo() {
        return this.domain + "api/save";
        //return this.domain + "api/save-user-tech-info.php";
    },
    get fixLinkClick() {
        return this.domain + "api/user-activity";
    },
    get fixPageLoad() {
        return this.domain + "api/user-activity";
    },
    get fixOnlineVisitor() {
        return this.domain + "api/user-activity";
    },
};
// ...
if (analyticApi.domain.search('.loc') !== -1) {
    console.info('current server: ', analyticApi.domain);
}

var TIME_VISITOR_SIGNAL = 2000;
// ...
var currentTime     = new Date();
var timezoneOffset  = new Date().getTimezoneOffset();
var colorDepth      = screen.colorDepth?screen.colorDepth:screen.pixelDepth;

var userX = detect.parse(navigator.userAgent);
// чи внесена інформація про оточення в БД
var wasSaved =  !!Cookies.get('visitorEnvironment');
// check user id?
var colUser = 'visitorId';
var cookieUserId = Cookies.get(colUser);
var userId = '';
if (cookieUserId !== undefined && cookieUserId !== "") {
    userId = cookieUserId;
} else {
    var idRandom =  generateUniqueId();
    Cookies.set(colUser, idRandom, { expires: 7 });
    userId = idRandom;
}

/*	Зібрати дані	*/
var cookiesEnabled = toDigitalBool(navigator.cookieEnabled);

var visitorInfo = {
    "domain": document.domain,
    "protocol": location.protocol,
    "pageUrl": window.location.href,
    "referrer": document.referrer,
    "userCookieId": userId,
    "time": currentTime,            // seconds || ms
    "timezone": timezoneOffset,
    "userAgent": navigator.userAgent,
    "OS": userX.os.name,
    "browser": userX.browser.name,
    "screenWidth": screen.width,
    "screenHeight": screen.height,
    "screenColorDepth": colorDepth,
    "cookiesEnabled": cookiesEnabled,
    "browserLanguage": navigator.language,
}
// ...
if (location.hash) {
    visitorInfo.anchorname = location.hash;
}
// ...
console.log('visitorId: ' + cookieUserId);
console.log('cookiesEnabled: ' + cookiesEnabled);

console.log('tech info', visitorInfo);
/*	Відправити дані на сервер	*/
// дані про оточення відвідувача сайту
if (!wasSaved) {
    sendInfoPack(visitorInfo, analyticApi.addTechInfo);
    console.info('Sending pack to server!');
}

// завантаження сторінки
sendInfoPack({
    //"domain": document.domain,
    "action": "page-load",
    "pageUrl": window.location.href,
    "referrer": document.referrer,
    "userCookieId": userId,
    "time": currentTime,
}, analyticApi.fixPageLoad);

// відправка повідомлень про присутність користувача
var signalCounter = 3 - 1;
var timerId = setInterval(function(){
    sendInfoPack({
            //"domain": document.domain,
            "action": "visitor-online",
            "pageUrl": window.location.href,
            "userCookieId": userId,
            "time": currentTime,
        }, analyticApi.fixOnlineVisitor);
    console.info('user is here!');
    // обмеження на кількість повідомлень
    if (!signalCounter--) {
        clearInterval(timerId);
    }
}, TIME_VISITOR_SIGNAL);

// обробка подій
// - клік по посиланню
window.addEventListener("click", function(event) {
    //console.log(event.target.getAttribute("href"));
    if (event.target.getAttribute("href")) {
        sendInfoPack({
            //"domain": document.domain,
            "action": "link-click",
            "pageUrl": window.location.href,
            "userCookieId": userId,
            "time": currentTime,
            "link-href": event.target.getAttribute("href"),
        }, analyticApi.fixLinkClick);
    }
});

/*  fns     */
function sendInfoPack(pack, urlReceiver) {
    $.ajax({
        type: "GET",
        dataType: "jsonp",
        url: urlReceiver,
        data: pack,
        // success: function(responsex) {
        //     console.log(responsex);
        //     alert('!!!');
        // }
    }).done(function() {
        console.log("request-status: success" );    // when dataType is json
    }).fail(function() {
        //console.log("request-status: error");
    });
}

function generateUniqueId() {
    return Date.now().toString(36) + Math.random().toString(36).substr(2, 5);
}

function toDigitalBool(value) {
    return !!value - 0;
}

})();
